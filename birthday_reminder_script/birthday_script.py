#!/usr/bin/python
# -*- encoding: utf-8 -*-

import csv
import time
from datetime import datetime, timedelta
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
import smtplib
import sys

_filename = 'birthday_file.csv'

# Validate command executes file validation
# Check command executes birthday reminders sending
_command = raw_input("Please enter the command (available commands: Validate, Check): ")

_subject = '''Birthday Reminder: %(name_of_birthday_person)s's birthday on %(date)s'''
_body = '''
Hi %(name)s,
This is a reminder that %(name_of_birthday_person)s will be celebrating their
birthday on %(date)s.
There are %(amount_of_days)s days left to get a present!
'''

# Set email from here
_from_email = 'your_email_here'


def get_server():
    # Setup connection to outgoing email server here
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login("username", "pass")
    return server


def validate_file(name, email, birth_date, line_no, today, errors_qty):
    if not name:
        errors_qty += 1
        print 'Name is not set in %s line.' % (line_no)
    if not email:
        errors_qty += 1
        print 'Email is not set in %s line.' % (line_no)
    # If birth date has no year
    if len(birth_date) == 5:
        try:
            # We don't know is birth year was a leap year, so 2008 year
            # is hardcoded here for successful file parse if parsed date is february 29
            datetime(2008, int(birth_date[:2]), int(birth_date[3:5]))
        except ValueError, e:
            errors_qty += 1
            print str(e) + ' in line %s' % (line_no)
    elif len(birth_date) == 10:
        try:
            date = datetime(int(birth_date[:4]), int(birth_date[5:7]), int(birth_date[8:10]))
            if date > today:
                errors_qty += 1
                print 'Birth date in line %s is not in the past' % (line_no)
        except ValueError, e:
            errors_qty += 1
            print str(e) + ' in line %s' % (line_no)
    else:
        errors_qty += 1
        print 'Birth date format in line %s is not correct ' \
              '(available formats: YYYY-MM-DD or MM-DD)' % (line_no)
    return errors_qty


def check_matched_birthdays(csvreader, today):
    error = 'Birth date format in some lines is not correct. ' \
            'Please run validation command ant try again.'
    upcoming_birthday = today + timedelta(days=7)
    matched_birthdays = []
    send_to = []
    for row in csvreader:
        if csvreader.line_num == 1:
            continue
        name = row[0]
        birth_date = row[2]
        birthday = False
        if len(birth_date) == 5:
            try:
                birthday = datetime(today.year, int(birth_date[:2]), int(birth_date[3:5]))
            except:
                if birth_date == '02-29':
                    print '%s does not have birthday this year :)' % name
                else:
                    sys.exit(error)
        elif len(birth_date) == 10:
            try:
                birthday = datetime(today.year, int(birth_date[5:7]), int(birth_date[8:10]))
            except:
                if birth_date[5:] == '02-29':
                    print '%s does not have birthday this year :)' % name
                else:
                    sys.exit(error)
        else:
            sys.exit(error)
        if birthday:
            if birthday.date() == upcoming_birthday.date():
                row.append(birthday.date())
                matched_birthdays.append(row)
            else:
                send_to.append(row)
    return matched_birthdays, send_to


def get_message(matched_birthday, name, email):
    vals = {
        'name_of_birthday_person': matched_birthday[0],
        'date': matched_birthday[3],
        'name': name,
        'amount_of_days': 7
    }
    subject, body = _subject % vals, _body % vals
    msg = MIMEMultipart()
    msg['From'] = _from_email
    msg['To'] = email
    msg['Subject'] = subject
    msg.attach(MIMEText(body, 'plain'))
    text = msg.as_string()
    return text


def send_email(send_to, matched_birthdays):
    for recipient in send_to:
        name = recipient[0]
        email = recipient[1]
        for matched_birthday in matched_birthdays:
            server = get_server()
            text = get_message(matched_birthday, name, email)
            # script will retry to send message 3 times
            tries = 3
            for i in range(tries):
                try:
                    server.sendmail(_from_email, email, text)
                except ValueError, e:
                    if i < tries - 1:
                        continue
                    else:
                        print e
                break

with open(_filename, 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    try:
        csvreader = csv.reader(csvfile, delimiter=',')
    except ValueError, e:
        print e
    today = datetime.today()
    if _command == 'Validate':
        line_no = 0
        errors_qty = 0
        for row in csvreader:
            line_no += 1
            if line_no == 1:
                continue
            errors_qty = validate_file(row[0], row[1], row[2], line_no, today, errors_qty)
        if errors_qty:
            print '\nData file is not valid, please check for %s error(s) above.' % errors_qty
        else:
            print '\nCongratulations, the file is valid.'
    elif _command == 'Check':
        matched_birthdays, send_to = check_matched_birthdays(csvreader, today)
        if matched_birthdays:
            send_email(send_to, matched_birthdays)
    else:
        print 'Command not found (available commands: Validate, Check)'
